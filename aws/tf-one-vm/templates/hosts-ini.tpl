[k8s-cluster:children]
kube-master
kube-node
[all]
anodot01 ansible_host=${node_ip} ip=${node_ip} etcd_member_name=etcd1 ansible_user=anodot ansible_become=yes ansible_ssh_private_key_file=/root/.ssh/anodot.key ansible_python_interpreter=/usr/bin/python
[kube-master]
anodot01
[kube-node]
anodot01
[etcd]
anodot01

