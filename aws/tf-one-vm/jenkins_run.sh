#!/usr/bin/env bash

set -e
set -u
set -x

TERRAFORM="docker run --rm --workdir=/terraform --volume=$(pwd):/terraform --user=$(id -u):$(id -g) hashicorp/terraform:0.12.3"
${TERRAFORM} init  -input=false -backend-config "key=${ENVIRONMENT}"
args=$( if [[ ${COMMAND} = plan ]] || [[ ${COMMAND} = apply ]] || [[ ${COMMAND} = destroy ]] ; then echo "-var environment=${ENVIRONMENT} -var=ami_id=${AMI_ID}" ; fi )
${TERRAFORM} ${COMMAND} ${args}  $( if [[ ${APPROVE} = true ]] ; then echo "-auto-approve" ; fi )
