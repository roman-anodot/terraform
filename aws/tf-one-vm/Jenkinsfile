
node('build') {
    ansiColor('xterm') {
    currentBuild.description = "${ENVIRONMENT} - ${COMMAND}"

    deleteDir()
    
    env.SSH_USER = "ubuntu"

    stage("ecr login"){
        sh "aws ecr get-login --region us-east-1 | sed 's#-e none##g' | bash"
    }
    // clone repo with Roman's creds
    checkout([$class: 'GitSCM', branches: [[name: '*/master']], userRemoteConfigs: [[credentialsId: 'd1ab4e61-f0a6-433e-89d8-3dd5e5e16f54', url: 'https://roman-anodot@bitbucket.org/anodotengineering/terraform.git']]])
    // run terraform code for VM spinup in AWS, tf files: aws/tf-one-vm
    dir('aws/tf-one-vm'){
        stage("Execute terraform"){
            sh '''
            set -x
            TERRAFORM="docker run --rm --workdir=/terraform --volume=$(pwd):/terraform --user=$(id -u):$(id -g) hashicorp/terraform:0.12.3"
            ${TERRAFORM} init  -input=false -backend-config "key=${ENVIRONMENT}"
            args=$( if [[ ${COMMAND} = plan ]] || [[ ${COMMAND} = apply ]] || [[ ${COMMAND} = destroy ]] ; then echo "-var environment=${ENVIRONMENT} -var ami_id=${AMI_ID} -var login_name=${SSH_USER}" ; fi )
            ${TERRAFORM} ${COMMAND} ${args} $( if [[ ${APPROVE} = true ]] ; then echo "-auto-approve" ; fi )
            '''
            }
        }
    

    if ("${COMMAND}" == "destroy"){
        sh '''
            aws --version
            #sudo apt-get -y install python3-pip
            #pip3 install awscli --upgrade --user
        '''
        sh "[[ ! -z \"${ENVIRONMENT}\" ]] && aws s3 rm s3://anodot-terraform/${ENVIRONMENT}" /* remove provided state from s3 bucket */
        sh "aws s3 ls s3://anodot-terraform/" /* list all states*/
        stage('Slack Notify') {
                    slackSend channel: '#onprem-one-vm-deployment',
                        color: 'warning',
                        message: "*${currentBuild.currentResult}:* Job ${env.BUILD_URL} \n ENV DESTROYED: http://${ENVIRONMENT}.ano-dev.com:8080/ \n"
                }
    }
    
    if ("${COMMAND}" == "apply"){
        // clone repo with Roman's creds
        checkout([$class: 'GitSCM', branches: [[name: '*/master']], userRemoteConfigs: [[credentialsId: 'd1ab4e61-f0a6-433e-89d8-3dd5e5e16f54', url: 'https://roman-anodot@bitbucket.org/roman-anodot/anodot-packaging.git']]])
    
        dir('tarball-sh'){
                stage("download packages"){
                    sh '''
                    # onprem-tf-one-host-roman/tarball-sh
                    # onprem-tf-one-host-roman/aws/tf-one-vm
                    cp ../aws/tf-one-vm/HOST.txt .
                    cp ../aws/tf-one-vm/hosts.ini .
                    ./download_packages.sh
                    echo ${BUILD_NUMBER} > "build.txt"
                    echo ${BRANCH} > branch.txt
                    '''
                }
                stage("Do tar (pull/save images)"){
                    sh '''
                    mkdir images && cd images
                    #docker info
                    #docker image ls
                    DOKCER_REPO="340481513670.dkr.ecr.us-east-1.amazonaws.com"
                    docker pull ${DOKCER_REPO}/anodot-kubespray:${BRANCH}
                    docker save ${DOKCER_REPO}/anodot-kubespray:${BRANCH} | gzip -c > spray-onprem-tar.tgz
                    docker rmi ${DOKCER_REPO}/anodot-kubespray:${BRANCH}
                    docker pull ${DOKCER_REPO}/anodot/webrepo:${BRANCH}
                    docker save ${DOKCER_REPO}/anodot/webrepo:${BRANCH} | gzip -c > webrepo-onprem-tar.tgz
                    docker rmi ${DOKCER_REPO}/anodot/webrepo:${BRANCH}
                    docker pull ${DOKCER_REPO}/registry-anodot:${BRANCH}
                    docker save ${DOKCER_REPO}/registry-anodot:${BRANCH} | gzip -c > registry-onprem-tar.tgz
                    docker rmi ${DOKCER_REPO}/registry-anodot:${BRANCH}
                    ls -la && cd -
                    tar --exclude='Jenkinsfile' --exclude='download_packages.sh' --exclude='extract.sh' -cvf anodotInstall_${BRANCH}.tar * 
                    du -sh *
                    '''
                }
                stage('S3 package save') {
                    sh '''
                        aws --version
                        sudo apt-get -y install python3-pip
                        pip3 install awscli --upgrade --user
                        echo "Uploading package from branch: ${BRANCH} to S3"
                        aws s3 cp anodotInstall_${BRANCH}.tar s3://anodot-packages/tarballs/
                    '''
                 }
                stage("Copy package to VM"){
                    withCredentials([sshUserPrivateKey(credentialsId: 'b6c26765-21f4-407b-b02a-1153c60c7b26', keyFileVariable: 'KEY')]) {
                        sh '''
                        HOST=$(cat HOST.txt)
                            scp -o StrictHostKeyChecking=no -i ${KEY} anodotInstall_${BRANCH}.tar ${SSH_USER}@${HOST}:/home/${SSH_USER}/
                            ssh -o StrictHostKeyChecking=no -i ${KEY} ${SSH_USER}@${HOST} "mkdir -pv anodotInstall_${BRANCH} && tar -xvf anodotInstall_${BRANCH}.tar -C anodotInstall_${BRANCH}"
                            # and clear package here in the jenkins to cleare 6Gb
                            rm -v anodotInstall_${BRANCH}.tar
                        '''
                    }
                }
                stage("Install Anodot in k8s"){
                    withCredentials([sshUserPrivateKey(credentialsId: 'b6c26765-21f4-407b-b02a-1153c60c7b26', keyFileVariable: 'KEY')]) {
                        sh '''
                        HOST=$(cat HOST.txt)
                            ssh -o StrictHostKeyChecking=no -i ${KEY} ${SSH_USER}@${HOST} "cd anodotInstall_${BRANCH} && sudo ./install.sh"
                            #ssh -o StrictHostKeyChecking=no -i ${KEY} ${SSH_USER}@${HOST} "cd anodotInstall_${BRANCH} && sudo systemctl restart anodocker.service"
                        '''
                    }
                }
                stage('Slack Notify cluster installed') {
                    env.HOST = readFile 'HOST.txt'
                    slackSend channel: '#onprem-one-vm-deployment',
                        color: 'good',
                        message: "*${currentBuild.currentResult}:* Job ${env.BUILD_URL} \n ENV CREATED: http://${ENVIRONMENT}.ano-dev.com/ \n SSH connection: \nssh -i ~/.ssh/k8s-staging.pem ${SSH_USER}@${ENVIRONMENT}.ano-dev.com"
                }
                stage("Check pods are running"){
                    withCredentials([sshUserPrivateKey(credentialsId: 'b6c26765-21f4-407b-b02a-1153c60c7b26', keyFileVariable: 'KEY')]) {
                        sh '''
                        HOST=$(cat HOST.txt)
                            ssh -o StrictHostKeyChecking=no -i ${KEY} ${SSH_USER}@${HOST} "sudo anodocker exec anodot-kubespray '/root/helm/utils/wait_pods.sh 1000 60'"
                        '''
                    }
                }
                stage("Install Testing pods"){
                    withCredentials([sshUserPrivateKey(credentialsId: 'b6c26765-21f4-407b-b02a-1153c60c7b26', keyFileVariable: 'KEY')]) {
                        sh '''
                        HOST=$(cat HOST.txt)
                            ssh -o StrictHostKeyChecking=no -i ${KEY} ${SSH_USER}@${HOST} "sudo anodocker exec anodot-kubespray '/root/helm/install-tests-helms.bash'"
                        '''
                    }
                }
                stage("Check pods are running"){
                    withCredentials([sshUserPrivateKey(credentialsId: 'b6c26765-21f4-407b-b02a-1153c60c7b26', keyFileVariable: 'KEY')]) {
                        sh '''
                        HOST=$(cat HOST.txt)
                            ssh -o StrictHostKeyChecking=no -i ${KEY} ${SSH_USER}@${HOST} "sudo anodocker exec anodot-kubespray '/root/helm/utils/wait_pods.sh 900 60'"
                        '''
                    }
                }
                stage('Slack Notify Starting tests') {
                    env.HOST = readFile 'HOST.txt'
                    slackSend channel: '#onprem-one-vm-deployment',
                        color: 'good',
                        message: "*${currentBuild.currentResult}:* Job ${env.BUILD_URL} \n STARTING TESTS: http://${ENVIRONMENT}.ano-dev.com/ \n SSH connection: \nssh -i ~/.ssh/k8s-staging.pem ${SSH_USER}@${ENVIRONMENT}.ano-dev.com"
                }
                stage("Start nodejs tests"){
                    withCredentials([sshUserPrivateKey(credentialsId: 'b6c26765-21f4-407b-b02a-1153c60c7b26', keyFileVariable: 'KEY')]) {
                        sh '''
                        HOST=$(cat HOST.txt)
                        echo "wait for 100 sec before start tests, make sure anodotautomations app started.."
                        sleep 100
                            ssh -o StrictHostKeyChecking=no -i ${KEY} ${SSH_USER}@${HOST} "sudo anodocker exec anodot-kubespray /root/helm/node-test.bash"
                        '''
                    }
                }
                stage("Get report from VM"){
                    withCredentials([sshUserPrivateKey(credentialsId: 'b6c26765-21f4-407b-b02a-1153c60c7b26', keyFileVariable: 'KEY')]) {
                        sh '''
                        HOST=$(cat HOST.txt)
                            ssh -o StrictHostKeyChecking=no -i ${KEY} ${SSH_USER}@${HOST} "sudo anodocker cp anodot-kubespray:/root/tests_report_onprem_smoke.xml ."
                            scp -o StrictHostKeyChecking=no -i ${KEY} ${SSH_USER}@${HOST}:/home/ubuntu/tests_report_onprem_smoke.xml .
                            ls -la 
                        '''
                    }
                }
                stage('Slack Notify All completed') {
                    env.HOST = readFile 'HOST.txt'
                    slackSend channel: '#onprem-one-vm-deployment',
                        color: 'good',
                        message: "*${currentBuild.currentResult}:* Job ${env.BUILD_URL} \n ALL COMPLETED: http://${ENVIRONMENT}.ano-dev.com/ \n SSH connection: \nssh -i ~/.ssh/k8s-staging.pem ${SSH_USER}@${ENVIRONMENT}.ano-dev.com"
                }
                stage('Email')
                {
                    env.ForEmailPlugin = env.WORKSPACE      
                    emailext attachmentsPattern: 'tests_report_onprem_smoke.xml',      
                    body: '''Test report attached''', 
                    subject: currentBuild.currentResult + " : " + env.JOB_NAME, 
                    to: 'roman@anodot.com, brian.clancy@anodot.com'
                }
                // stage("send email") {
                //     emailext body: 'Anodot URL: http://${ENVIRONMENT_NAME}.ano-dev.com:8080/ \n\n Check console output at ${BUILD_URL}console to view the results.', recipientProviders: [[$class: 'DevelopersRecipientProvider'], [$class: 'RequesterRecipientProvider']], subject: '$PROJECT_NAME - $BUILD_STATUS'
                //}
                
                // stage('TODO: install automation test') {
                //     sh "helm install automations with slack notify"
                // }
                // stage('TODO: run tests and get report') {
                //     sh "run tests and get report by email or slack"
                // }
                // stage('TODO: Upload anodot-package to S3') {
                //     sh "Upload anodot-package to S3 on weekly basis"
                // }
            }
        }
    }
}
