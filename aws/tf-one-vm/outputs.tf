//output "private_subnets" {
//  value = "${module.vpc.private_subnets}"
//}
//
//output "public_subnets" {
//  value = "${module.vpc.public_subnets}"
//}
//
//output "vpc_id" {
//  value = "${module.vpc.vpc_id}"
//}
//
//output "public_route_table_ids" {
//  value = "${module.vpc.public_route_table_ids}"
//}
//
//output "private_route_table_ids" {
//  value = "${module.vpc.private_route_table_ids}"
//}
//
//output "default_security_group_id" {
//  value = "${module.vpc.default_security_group_id}"
//}

# output "nat_eips" {
#   value = "${module.vpc.nat_eips}"
# }
# 
# output "nat_eips_public_ips" {
#   value = "${module.vpc.nat_eips_public_ips}"
# }

//output "natgw_ids" {
//  value = "${module.vpc.natgw_ids}"
//}
//
//output "igw_id" {
//  value = "${module.vpc.igw_id}"
//}

# output "ansible_iventory_file" {
#   value = "${data.template_file.inventory.rendered}"
# }

output "host_public_ip" {
  value = "${aws_instance.k8s_host.public_ip}"
}

output "k8s_host_privat_ips" {
  value = "${aws_instance.k8s_host.*.private_ip}"
}

#output "k8s_node_connect" {
#  value = "\nssh -i ~/.ssh/${var.key_name}.pem -o 'ProxyCommand ssh -i ~/.ssh/${var.key_name}.pem ${var.login_name}@${aws_instance.k8s_bastion.public_ip} -W %h:%p' ${var.login_name}@${aws_instance.k8s_nodes.*.private_ip[0]}\n"
#}

output "k8s_host_connect" {
  value = "\nssh -i ~/.ssh/${var.key_name}.pem ${var.login_name}@${aws_instance.k8s_host.public_ip}\n"
}

#output "ssh_config_entry" {
#  value = "\nHost k8s-node-${var.environment}-1\n  Hostname ${aws_instance.k8s_nodes.*.private_ip[0]}\n  ProxyCommand ssh ${var.login_name}@${aws_instance.k8s_bastion.public_ip} -W %h:%p\n  User ${var.login_name}\n  IdentityFile ~/.ssh/${var.key_name}.pem\n"
#}
output "anodot_URL" {
  value = "http://${aws_route53_record.dns_record.fqdn}/\n"
}

// output "network_interfaces" {
//   value = "${aws_instance.k8s_host.network_interface_id}"
// }

output "primary_network_interface_id" {
  description = "ID of the instance's primary network interface"
  value       = "${aws_instance.k8s_host.*.primary_network_interface_id}"
}
