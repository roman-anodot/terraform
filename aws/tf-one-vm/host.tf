//resource "aws_eip" "k8s_host_eip" {
//  instance = "${aws_instance.k8s_host.id}"
//  vpc      = true
//}

resource "aws_security_group" "k8s_host_sg" {
  name        = "k8s-host-${var.environment}"
  description = "Allow only ssh traffic"
  vpc_id      = "${var.vpc_id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "6"
    cidr_blocks = "${var.ssh_http_source_address}"
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "6"
    cidr_blocks = "${var.ssh_http_source_address}"
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "6"
    cidr_blocks = "${var.ssh_http_source_address}"
  }

  # ingress {
  #   from_port   = 0
  #   to_port     = 0
  #   protocol    = "-1"
  #   security_groups = [ "${aws_security_group.k8s_node_sg.id}" ]
  # }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["10.0.0.0/24"] # "0.0.0.0/0" for open internet
  }

# open 25 port for smtp work.
  egress {
    from_port   = 25
    to_port     = 25
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] 
  }

  tags =  {
    Name = "k8s-host-${var.environment}"
    type = "onprem"
  }
}

resource "aws_instance" "k8s_host" {
  ami                         = "${var.ami_id}"
  key_name                    = "${var.key_name}"
  instance_type               = "${var.k8s_host_instance_type}"
  vpc_security_group_ids      = ["${aws_security_group.k8s_host_sg.id}"]
  subnet_id                   = "${var.subnet_id}"
  associate_public_ip_address = true
  iam_instance_profile        = "${var.host_profile}"

  root_block_device {
    delete_on_termination = true
    encrypted             = false
    iops                  = 500
    volume_size           = 50
    volume_type           = "gp3"
    throughput            = 150
    tags                  = { type = "onprem" }
  }

  tags = {
    Name        = "k8s-host-${var.environment}"
    environment = "${var.environment}"
    loginId = "${var.login_name}"
    type = "onprem"
  }
}
