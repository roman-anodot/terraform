variable "aws_region" {
  default = "us-east-1"
}

//variable "num_az" {
//  default = "1"
//}

variable "k8s_host_instance_type" {
  default = "m5.4xlarge"
}

//variable "k8s_node_type" {
//  default = "m4.2xlarge"
//  default = "m4.large"
//}

variable "k8s_host_number" {
  default = 3
}

variable "host_profile" {
  default = "ECS_Pull"
}

variable "environment" {
  type = "string"
}

variable "key_name" {
  default = "k8s-staging"
}

variable "vpc_id" {
  default = "vpc-6f00ab09"
}

variable "subnet_id" {
  default = "subnet-271a7c7c"
}

// variable "node_root_block_size_gb" {
//   default = 100
// }

//variable "vpc_cidr" {
//  type = "string"
//}
//
//variable "private_cidr" {
//  type    = "list"
//}
//
//variable "public_cidr" {
//  type    = "list"
//}

//variable "ami_owner" {
//  default = "099720109477"
//}

variable "ami_id" {
  #default = "ami-04169656fea786776" # some ubuntu
  default = "ami-00552bd39464c2e3a" # RHEL 7.6
}

//variable "key_ext" {
//  default = "key.pub"
//}

//variable "s3_log_backet" {
//  default = "elb-logs"
//}

//variable "app_source_address" {
//  type = "list"
//
//  // Israel office, Testim.io
//  default = ["82.80.54.55/32", "52.186.64.155/32", "13.92.209.21/32", "13.92.213.50/32", "54.76.232.124/32"]
//}

variable "ssh_http_source_address" {
  type = "list"

  // Jenkins, Israel office
  // 3.211.145.102/32 - jenkins slave NAT IP
  default = ["146.185.63.42/32", "188.120.135.18/32", "62.216.42.54/32", "37.26.149.155/32", "46.116.184.19/32", "199.203.250.201/32", "3.211.145.102/32", "178.133.228.84/32", "193.0.220.16/32", "109.86.87.0/24", "77.125.3.108/32", "192.168.190.0/24", "93.74.228.251/32"]
}

// Target account for peering
//variable "perring_destination_account_id" {
//  type = "string"
//}

// Target VPC for peering
//variable "peering_destination_vpc" {
//  type = "string"
//}

//// Target VPC for peering
//variable "peering_destination_sg" {
//  type = "string"
//}

// Target network to route
//variable "peering_route_network" {
//  type = "string"
//}

//variable "ssl_certificate" {
//  type = "string"
//}

//variable "data_device" {
//  default = "/dev/xvdf"
//}

//variable "data_mountpoint" {
//  default = "/DATA"
//}

//variable "data_size" {
//  default = "500"
//}

// Data snapshot id
//variable "data_snapshot" {
//  type = "string"
//}

//variable "peering_enabled" {
//  type = "string"
//}

//variable "kms_key" {
//  type = "string"
//}

//variable "app_ami" {
//  default = "ami-04169656fea786776"
//}

//variable "environment_iam_roles" {
//  type = "string"
//}


//variable "route_53_zone_id" {
//  type = "string"
//}
//
//variable "route_53_base_domain_name" {
//  type = "string"
//}


variable "nodes_night_off" {
  default = "true"
}

//variable "bastion_private_key_name" {
//  default = "bastion.pem"
//}

variable "login_name" {
  default = "ubuntu" # for RHEL 6.5+ - ec2-user
}

//used to proxy http requests from bastion hosts to k8s inress nodes.
//variable "ha_proxy_listen_port" {
//  default = 8080
//}

//variable "account_id" {
//  type = "string"
//}
//
//variable "bastion_init" {
//  type = "string"
//  default =<<EOF
//#!/bin/bash
//systemctl enable idle_shutdown.service
//systemctl start  idle_shutdown.service
//EOF
//}
//

