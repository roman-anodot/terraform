resource "aws_route53_record" "dns_record" {
  zone_id = "ZF6LQJ4VS1LNQ" # ano-dev.com - HostedZone created by Route53 Registrar
  name    = "${var.environment}"
  type    = "A"
  ttl     = "300"
  records = ["${aws_instance.k8s_host.public_ip}"]
}
